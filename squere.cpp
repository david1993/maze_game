#include "squere.h"

#include <QPalette>
#include <QDebug>

squere::squere(const coordinate & co, QWidget *parent)
    : QLabel(parent), m_coordinate(co)
{
    setStyleSheet("QLabel { background-color : black;}");
    setMouseTracking(true);
}

squere::~squere()
{

}

void squere::mouseReleaseEvent(QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton)
    {
        emit source_changed(m_coordinate);
    }
    if (event->button() == Qt::RightButton)
    {
        emit destination_changed(m_coordinate);
    }
}
