#-------------------------------------------------
#
# Project created by QtCreator 2017-02-25T15:48:23
#
#-------------------------------------------------

QT       += core gui
QT       += testlib
QT       += concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Maze_Game
TEMPLATE = app


SOURCES += main.cpp\
    maze.cpp \
    squere.cpp \
    main_widget.cpp

HEADERS  += \
    coordinate.h \
    maze.h \
    ship.h \
    squere.h \
    main_widget.h
