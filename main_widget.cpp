#include "main_widget.h"

#include <time.h>

#include <QVBoxLayout>
#include <QThread>

maze *main_widget::generate_maze()
{
    srand (time(NULL));
    int width = 4 + ( std::rand() % ( 50 - 4 + 1 ) );
    //int height = width - 5 + ( std::rand() % ( width + 5 ) );
    int height = width;
    int obstacles_number = std::rand() % (width * height)/2;

    std::vector<coordinate> obstacles;

    for (int t = 0; t < obstacles_number; ++t)
    {
        int i = std::rand() % height;
        int j = std::rand() % width;

        obstacles.push_back(coordinate(i, j));
    }

    return new maze(width, height, obstacles);
}

main_widget::main_widget(QWidget *parent) : QWidget(parent)
{
    resize(800, 800);
    m_maze = generate_maze();
    setFocusPolicy(Qt::ClickFocus);

    m_main = new QVBoxLayout(this);
    m_main->addWidget(m_maze);
}

void main_widget::keyPressEvent(QKeyEvent  * ev)
{
    if (ev->key() == Qt::Key_Space)
    {
        for (int i = 0; i < m_remained_mazes.size(); ++i)
        {
            delete m_remained_mazes[i];
        }
        m_remained_mazes.clear();
        m_main->removeWidget(m_maze);
        maze* curr = m_maze;
        m_maze = generate_maze();
        m_main->addWidget(m_maze);
        curr->set_there_is_path(0);
        curr->close();
        m_remained_mazes.push_back(curr);
    }
}
