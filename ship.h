#ifndef SHIP_H
#define SHIP_H

#include "coordinate.h"

struct ship
{
    coordinate m_head;
    coordinate m_mid;

    ship() : m_head(coordinate(-1, -1)), m_mid(coordinate(-1, -1)) {}
    ship(const coordinate& head, const coordinate& mid) : m_head(head), m_mid(mid) {}

    bool operator==(const ship& other)
    {
      return m_head == other.m_head && m_mid == other.m_mid;
    }

};
#endif // SHIP_H
