#ifndef COORDINATE_H
#define COORDINATE_H

#include <QObject>

struct coordinate
{
    int m_i;
    int m_j;

    coordinate() : m_i(0), m_j(0) {}
    coordinate(const int& i, const int& j) : m_i(i), m_j(j) {}

    friend bool operator<(const coordinate& l, const coordinate& r)
    {
        if (l.m_i < r.m_i)  return true;
        if (l.m_i > r.m_i)  return false;

        if (l.m_j < r.m_j)  return true;
        if (l.m_j > r.m_j)  return false;

        return false;
    }

    bool operator==(const coordinate& other)
    {
      return m_i == other.m_i && m_j == other.m_j;
    }

    bool operator!=(const coordinate& other)
    {
        return m_i != other.m_i || m_j != other.m_j;
    }
};

#endif // COORDINATE_H
