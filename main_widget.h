#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>
#include <queue>

#include "maze.h"

class QVBoxLayout;

class main_widget : public QWidget
{
    Q_OBJECT

private:
    maze* m_maze;
    std::queue<maze*> m_mazes;
    QVBoxLayout* m_main;
    std::vector<maze*> m_remained_mazes;

    maze* generate_maze();

public:
    explicit main_widget(QWidget *parent = 0);

    void keyPressEvent(QKeyEvent*);
};

#endif // MAIN_WIDGET_H
