#ifndef SQUERE_H
#define SQUERE_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>

#include "coordinate.h"

class squere : public QLabel
{
    Q_OBJECT

private:
    coordinate m_coordinate;

public:
    squere(const coordinate&, QWidget *parent = 0);
    ~squere();

    void mouseReleaseEvent(QMouseEvent*);

signals:
    void source_changed(coordinate);
    void destination_changed(coordinate);
};

#endif // SQUERE_H
