#include "maze.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QMessageBox>
#include <QApplication>
#include <map>
#include <queue>
#include <stack>
#include <QTest>
#include <QAbstractEventDispatcher>
#include <QtConcurrent>

bool maze::is_available(const coordinate & squere)
{
    if (squere.m_i >=0 && squere.m_i < m_maze_height && squere.m_j >= 0 && squere.m_j < m_maze_width)
        return m_maze[squere.m_i][squere.m_j] != -1;
    return false;
}

bool maze::is_rotateable(const ship & object)
{
    if (object.m_mid.m_j == object.m_head.m_j)
    {
        coordinate left = coordinate(object.m_mid.m_i, object.m_mid.m_j - 1);
        coordinate right = coordinate(object.m_mid.m_i, object.m_mid.m_j + 1);
        coordinate left_down = coordinate(object.m_mid.m_i + 1, object.m_mid.m_j - 1);
        coordinate left_up = coordinate(object.m_mid.m_i - 1, object.m_mid.m_j - 1);
        coordinate right_down = coordinate(object.m_mid.m_i + 1, object.m_mid.m_j + 1);
        coordinate right_up = coordinate(object.m_mid.m_i - 1, object.m_mid.m_j + 1);

        return (is_available(left) && is_available(right) && is_available(left_up) && is_available(right_down))
                || (is_available(left) && is_available(right) && is_available(left_down) && is_available(right_up));
    }

    if (object.m_mid.m_i == object.m_head.m_i)
    {
        coordinate up = coordinate(object.m_mid.m_i - 1, object.m_mid.m_j);
        coordinate down = coordinate(object.m_mid.m_i + 1, object.m_mid.m_j);
        coordinate left_down = coordinate(object.m_mid.m_i + 1, object.m_mid.m_j - 1);
        coordinate left_up = coordinate(object.m_mid.m_i - 1, object.m_mid.m_j - 1);
        coordinate right_down = coordinate(object.m_mid.m_i + 1, object.m_mid.m_j + 1);
        coordinate right_up = coordinate(object.m_mid.m_i - 1, object.m_mid.m_j + 1);

        return (is_available(up) && is_available(down) && is_available(left_up) && is_available(right_down))
                || (is_available(up) && is_available(down) && is_available(left_down) && is_available(right_up));
    }
}

bool maze::find_path()
{
    if (m_ship_1 == m_ship_2)
    {
        if (path_finding_core(m_ship_1))
            return true;
    }
    else
    {
        if (path_finding_core(m_ship_1))
            return true;
        return path_finding_core(m_ship_2);
    }
}

bool maze::path_finding_core(const ship & obj)
{
    m_path = std::stack<coordinate>();
    if (m_dst == m_src)
    {
        m_path.push(m_dst);
        return true;
    }

    std::queue<ship> q;
    q.push(obj);

    ship current;
    std::vector<coordinate> dst_neighbours = get_dst_neighbours();

    while(!q.empty())
    {
        current = q.front();
        q.pop();

        for (auto& i : dst_neighbours)
        {
            if (i == m_src)
            {
                m_path.push(m_dst);
                m_path.push(i);
                return true;
            }
            if (i == current.m_mid)
            {
                if ((current.m_head.m_i == current.m_mid.m_i && m_dst.m_i == current.m_head.m_i) ||
                        (current.m_head.m_j == current.m_mid.m_j && m_dst.m_j == current.m_head.m_j)
                        || (m_dst.m_i == current.m_mid.m_i && current.m_head.m_j == current.m_mid.m_j && is_rotateable(ship(current.m_head, current.m_mid)))
                        || (m_dst.m_j == current.m_mid.m_j && current.m_head.m_i == current.m_mid.m_i && is_rotateable(ship(current.m_head, current.m_mid))))

                {

                    m_path.push(m_dst);
                    m_path.push(current.m_mid);

                    while(1)
                    {
                        if (check_neighbour(current.m_mid, coordinate(current.m_mid.m_i - 1, current.m_mid.m_j)))
                        {
                            m_path.push(coordinate(current.m_mid.m_i - 1, current.m_mid.m_j));
                            current.m_mid = coordinate(current.m_mid.m_i - 1, current.m_mid.m_j);
                            if (coordinate(current.m_mid.m_i, current.m_mid.m_j) == m_src)
                                break;
                        }
                        else
                        {
                            if (check_neighbour(current.m_mid, coordinate(current.m_mid.m_i + 1, current.m_mid.m_j)))
                            {
                                m_path.push(coordinate(current.m_mid.m_i + 1, current.m_mid.m_j));
                                current.m_mid = coordinate(current.m_mid.m_i + 1, current.m_mid.m_j);
                                if (coordinate(current.m_mid.m_i, current.m_mid.m_j) == m_src)
                                    break;
                            }
                            else
                            {
                                if (check_neighbour(current.m_mid, coordinate(current.m_mid.m_i, current.m_mid.m_j - 1)))
                                {
                                    m_path.push(coordinate(current.m_mid.m_i, current.m_mid.m_j - 1));
                                    current.m_mid = coordinate(current.m_mid.m_i, current.m_mid.m_j - 1);
                                    if (coordinate(current.m_mid.m_i, current.m_mid.m_j) == m_src)
                                        break;
                                }
                                else
                                {
                                    if (check_neighbour(current.m_mid, coordinate(current.m_mid.m_i, current.m_mid.m_j + 1)))
                                    {
                                        m_path.push(coordinate(current.m_mid.m_i, current.m_mid.m_j + 1));
                                        current.m_mid = coordinate(current.m_mid.m_i, current.m_mid.m_j + 1);
                                        if (coordinate(current.m_mid.m_i, current.m_mid.m_j) == m_src)
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(!m_path.empty())
            {
                return true;
            }
        }
        if (current.m_mid.m_j == current.m_head.m_j)
        {
            if (is_available(coordinate(current.m_mid.m_i - 2, current.m_mid.m_j)) && is_available(coordinate(current.m_mid.m_i - 1, current.m_mid.m_j)))
            {
                if (m_maze[current.m_mid.m_i - 1][current.m_mid.m_j] == 0 || m_maze[current.m_mid.m_i - 1][current.m_mid.m_j] == -2)
                {
                    if (coordinate(current.m_mid.m_i - 1, current.m_mid.m_j) != m_src)
                    {
                        q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i - 1, current.m_mid.m_j)));
                        m_maze[current.m_mid.m_i - 1][current.m_mid.m_j] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                    }
                }
            }
            if (is_available(coordinate(current.m_mid.m_i + 2, current.m_mid.m_j)) && is_available(coordinate(current.m_mid.m_i + 1, current.m_mid.m_j)))
            {
                if (m_maze[current.m_mid.m_i + 1][current.m_mid.m_j] == 0 || m_maze[current.m_mid.m_i + 1][current.m_mid.m_j] == -2)
                {
                    if (coordinate(current.m_mid.m_i + 1, current.m_mid.m_j) != m_src)
                    {
                        q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i + 1, current.m_mid.m_j)));
                        m_maze[current.m_mid.m_i + 1][current.m_mid.m_j] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                    }
                }
            }
            if(is_rotateable(current))
            {
                if (is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j - 2)) && is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j - 1)))
                {
                    if (m_maze[current.m_mid.m_i][current.m_mid.m_j - 1] == 0 || m_maze[current.m_mid.m_i][current.m_mid.m_j - 1] == -2)
                    {
                        if (coordinate(current.m_mid.m_i, current.m_mid.m_j - 1) != m_src)
                        {
                            q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i, current.m_mid.m_j - 1)));
                            m_maze[current.m_mid.m_i][current.m_mid.m_j - 1] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                        }
                    }
                }
                if (is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j + 2)) && is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j + 1)))
                {
                    if (m_maze[current.m_mid.m_i][current.m_mid.m_j + 1] == 0 || m_maze[current.m_mid.m_i][current.m_mid.m_j + 1] == -2)
                    {
                        if (coordinate(current.m_mid.m_i, current.m_mid.m_j + 1) != m_src)
                        {
                            q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i, current.m_mid.m_j + 1)));
                            m_maze[current.m_mid.m_i][current.m_mid.m_j + 1] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                        }
                    }
                }
            }
        }

        if (current.m_mid.m_i == current.m_head.m_i)
        {
            if (is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j - 2)) && is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j - 1)))
            {
                if (m_maze[current.m_mid.m_i][current.m_mid.m_j - 1] == 0 || m_maze[current.m_mid.m_i][current.m_mid.m_j - 1] == -2)
                {
                    if (coordinate(current.m_mid.m_i, current.m_mid.m_j - 1) != m_src)
                    {
                        q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i, current.m_mid.m_j - 1)));
                        m_maze[current.m_mid.m_i][current.m_mid.m_j - 1] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                    }
                }
            }
            if (is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j + 2)) && is_available(coordinate(current.m_mid.m_i, current.m_mid.m_j + 1)))
            {
                if (m_maze[current.m_mid.m_i][current.m_mid.m_j + 1] == 0 || m_maze[current.m_mid.m_i][current.m_mid.m_j + 1] == -2)
                {
                    if (coordinate(current.m_mid.m_i, current.m_mid.m_j + 1) != m_src)
                    {
                        q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i, current.m_mid.m_j + 1)));
                        m_maze[current.m_mid.m_i][current.m_mid.m_j + 1] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                    }
                }
            }
            if(is_rotateable(current))
            {
                if (is_available(coordinate(current.m_mid.m_i - 2, current.m_mid.m_j)) && is_available(coordinate(current.m_mid.m_i - 1, current.m_mid.m_j)))
                {
                    if (m_maze[current.m_mid.m_i - 1][current.m_mid.m_j] == 0 || m_maze[current.m_mid.m_i - 1][current.m_mid.m_j] == -2)
                    {
                        if (coordinate(current.m_mid.m_i - 1, current.m_mid.m_j) != m_src)
                        {
                            q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i - 1, current.m_mid.m_j)));
                            m_maze[current.m_mid.m_i - 1][current.m_mid.m_j] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                        }
                    }
                }
                if (is_available(coordinate(current.m_mid.m_i + 2, current.m_mid.m_j)) && is_available(coordinate(current.m_mid.m_i + 1, current.m_mid.m_j)))
                {
                    if (m_maze[current.m_mid.m_i + 1][current.m_mid.m_j] == 0 || m_maze[current.m_mid.m_i + 1][current.m_mid.m_j] == -2)
                    {
                        if (coordinate(current.m_mid.m_i + 1, current.m_mid.m_j) != m_src)
                        {
                            q.push(ship(coordinate(current.m_mid.m_i, current.m_mid.m_j), coordinate(current.m_mid.m_i + 1, current.m_mid.m_j)));
                            m_maze[current.m_mid.m_i + 1][current.m_mid.m_j] = m_maze[current.m_mid.m_i][current.m_mid.m_j] + 1;
                        }
                    }
                }
            }
        }
    }
    return false;
}

std::vector<coordinate> maze::get_dst_neighbours()
{
    std::vector<coordinate> neighbours;
    if (m_dst.m_i != -1)
    {
        if (is_available(coordinate(m_dst.m_i - 1, m_dst.m_j)) && is_available(coordinate(m_dst.m_i - 2, m_dst.m_j)))
        {
            m_maze[m_dst.m_i - 1][m_dst.m_j] = -2;
            neighbours.push_back(coordinate(m_dst.m_i - 1, m_dst.m_j));
        }

        if (is_available(coordinate(m_dst.m_i + 1, m_dst.m_j)) && is_available(coordinate(m_dst.m_i + 2, m_dst.m_j)))
        {
             m_maze[m_dst.m_i + 1][m_dst.m_j] = -2;
             neighbours.push_back(coordinate(m_dst.m_i + 1, m_dst.m_j));
        }

        if (is_available(coordinate(m_dst.m_i, m_dst.m_j - 1)) && is_available(coordinate(m_dst.m_i, m_dst.m_j - 2)))
        {
            m_maze[m_dst.m_i][m_dst.m_j - 1] = -2;
            neighbours.push_back(coordinate(m_dst.m_i, m_dst.m_j - 1));
        }

        if (is_available(coordinate(m_dst.m_i, m_dst.m_j + 1)) && is_available(coordinate(m_dst.m_i, m_dst.m_j + 2)))
        {
            m_maze[m_dst.m_i][m_dst.m_j + 1] = -2;
            neighbours.push_back(coordinate(m_dst.m_i, m_dst.m_j + 1));
        }
    }

    return neighbours;
}

bool maze::check_neighbour(const coordinate & current, const coordinate & neighbour)
{
    if (!is_available(neighbour) || (m_maze[neighbour.m_i][neighbour.m_j] == 0 && m_src != neighbour))
        return false;
    if (current.m_i == neighbour.m_i)
    {
        if (current.m_j == neighbour.m_j - 1)
        {
            if (m_maze[current.m_i][current.m_j] == m_maze[neighbour.m_i][neighbour.m_j] + 1)
            {
                if (is_available(coordinate(neighbour.m_i, neighbour.m_j + 1)))
                {
                        return true;
                }
                if (is_available(coordinate(neighbour.m_i - 1, neighbour.m_j)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i - 1, neighbour.m_j), neighbour)))
                    {
                            return true;
                    }
                }
                if (is_available(coordinate(neighbour.m_i + 1, neighbour.m_j)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i + 1, neighbour.m_j), neighbour)))
                    {
                            return true;
                    }
                }
            }
        }

        if (current.m_j == neighbour.m_j + 1)
        {
            if (m_maze[current.m_i][current.m_j] == m_maze[neighbour.m_i][neighbour.m_j] + 1)
            {
                if (is_available(coordinate(neighbour.m_i, neighbour.m_j - 1)))
                {
                        return true;
                }
                if (is_available(coordinate(neighbour.m_i - 1, neighbour.m_j)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i - 1, neighbour.m_j), neighbour)))
                    {
                            return true;
                    }
                }
                if (is_available(coordinate(neighbour.m_i + 1, neighbour.m_j)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i + 1, neighbour.m_j), neighbour)))
                    {
                            return true;
                    }
                }
            }
        }
    }

    if (current.m_j == neighbour.m_j)
    {
        if (current.m_i == neighbour.m_i - 1)
        {
            if (m_maze[current.m_i][current.m_j] == m_maze[neighbour.m_i][neighbour.m_j] + 1)
            {
                if (is_available(coordinate(neighbour.m_i + 1, neighbour.m_j)))
                {
                        return true;
                }
                if (is_available(coordinate(neighbour.m_i, neighbour.m_j - 1)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i, neighbour.m_j - 1), neighbour)))
                    {
                            return true;
                    }
                }
                if (is_available(coordinate(neighbour.m_i, neighbour.m_j + 1)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i, neighbour.m_j + 1), neighbour)))
                    {
                            return true;
                    }
                }
            }
        }

        if (current.m_i == neighbour.m_i + 1)
        {
            if (m_maze[current.m_i][current.m_j] == m_maze[neighbour.m_i][neighbour.m_j] + 1)
            {
                if (is_available(coordinate(neighbour.m_i - 1, neighbour.m_j)))
                {
                        return true;
                }
                if (is_available(coordinate(neighbour.m_i, neighbour.m_j - 1)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i, neighbour.m_j - 1), neighbour)))
                    {
                            return true;
                    }
                }
                if (is_available(coordinate(neighbour.m_i, neighbour.m_j + 1)))
                {
                    if (is_rotateable(ship(coordinate(neighbour.m_i, neighbour.m_j + 1), neighbour)))
                    {
                            return true;
                    }
                }
            }
        }
    }

    return false;
}

void maze::start_ship_movement(const std::stack<coordinate> & path)
{
    if (!m_there_is_path)
        return;
    std::stack<coordinate> path_cpy = path;
    if (path_cpy.size() == 1)
    {
        while(1)
        {
            coordinate curr = path.top();
            path_cpy.pop();
            QTest::qWait(100);
            if (!m_there_is_path)
                return;
            if (is_available(coordinate(curr.m_i, curr.m_j + 1)) && is_available(coordinate(curr.m_i, curr.m_j - 1)))
            {
                (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j + 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j - 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
            }
            else
            {
                (m_maze_squeres.find(coordinate(curr.m_i + 1, curr.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                (m_maze_squeres.find(coordinate(curr.m_i - 1, curr.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
            }
            path_cpy = path;
        }
    }

    ship current_pos;

    while(!path_cpy.empty())
    {
        if(path_cpy.size() == path.size())
        {
            if (path.size() != 1 && path.size() != 2)
                while(!path_cpy.empty())
                {
                    QTest::qWait(5);
                    if (!m_there_is_path)
                        return;
                    (m_maze_squeres.find(path_cpy.top())->second)->setStyleSheet("QLabel { background-color : green;}");
                    path_cpy.pop();
                }
            if (path.size() == 2)
            {
                QTest::qWait(5);
                if (!m_there_is_path)
                    return;
                //(m_maze_squeres.find(path_cpy.top())->second)->setStyleSheet("QLabel { background-color : green;}");
                path_cpy.pop();
            }
            path_cpy = path;
            current_pos = ship();
        }
        if (current_pos.m_head.m_i == -1)
        {
            coordinate mid = path_cpy.top();
            path_cpy.pop();
            coordinate head = path_cpy.top();
            path_cpy.pop();
            if (mid.m_i == head.m_i)
            {
                if (mid.m_j == head.m_j + 1)
                {
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    if (head == m_dst)
                    {
                        path_cpy = path;
                    }
                    else
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : black;}");
                        (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j - 2))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_head = coordinate(mid.m_i, mid.m_j - 2);
                        current_pos.m_mid = head;
                    }
                }

                if (mid.m_j == head.m_j - 1)
                {
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    if (head == m_dst)
                    {
                        path_cpy = path;
                    }
                    else
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : black;}");
                        (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j + 2))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_head = coordinate(mid.m_i, mid.m_j + 2);
                        current_pos.m_mid = head;
                    }
                }
            }

            if (mid.m_j == head.m_j)
            {
                if (mid.m_i == head.m_i + 1)
                {
                    (m_maze_squeres.find(coordinate(mid.m_i + 1, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i - 1, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    if (head == m_dst)
                    {
                        path_cpy = path;
                    }
                    else
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(mid.m_i + 1, mid.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                        (m_maze_squeres.find(coordinate(mid.m_i - 2, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_head = coordinate(mid.m_i - 2, mid.m_j);
                        current_pos.m_mid = head;
                    }
                }

                if (mid.m_i == head.m_i - 1)
                {
                    (m_maze_squeres.find(coordinate(mid.m_i + 1, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i - 1, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    (m_maze_squeres.find(coordinate(mid.m_i, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                    if (head == m_dst)
                    {
                        path_cpy = path;
                    }
                    else
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(mid.m_i - 1, mid.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                        (m_maze_squeres.find(coordinate(mid.m_i + 2, mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_head = coordinate(mid.m_i + 2, mid.m_j);
                        current_pos.m_mid = head;
                    }
                }
            }
        }
        else
        {
            coordinate curr = path_cpy.top();
            path_cpy.pop();

            if (curr == current_pos.m_head)
            {
                if(curr == m_dst)
                    path_cpy = path;
                else if (current_pos.m_mid.m_i == current_pos.m_head.m_i)
                {
                    if (current_pos.m_mid.m_j == current_pos.m_head.m_j + 1)
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : green;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 2))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_mid = current_pos.m_head;
                        current_pos.m_head = coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 1);
                    }

                    if (current_pos.m_mid.m_j == current_pos.m_head.m_j - 1)
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : green;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 2))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_mid = current_pos.m_head;
                        current_pos.m_head = coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 1);
                    }
                }

                else if (current_pos.m_mid.m_j == current_pos.m_head.m_j)
                {
                    if (current_pos.m_mid.m_i == current_pos.m_head.m_i + 1)
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i + 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i - 2, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_mid = current_pos.m_head;
                        current_pos.m_head = coordinate(current_pos.m_mid.m_i - 1, current_pos.m_mid.m_j);
                    }

                    if (current_pos.m_mid.m_i == current_pos.m_head.m_i - 1)
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i - 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i + 2, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        current_pos.m_mid = current_pos.m_head;
                        current_pos.m_head = coordinate(current_pos.m_mid.m_i + 1, current_pos.m_mid.m_j);
                    }
                }
            }
            else
            {
                if (current_pos.m_head.m_i == current_pos.m_mid.m_i)
                {
                    if (curr.m_i < current_pos.m_mid.m_i)
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        if (current_pos.m_head.m_j > current_pos.m_mid.m_j)
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j - 2))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j + 2))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i + 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i - 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");

                        if (curr == m_dst)
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            path_cpy = path;
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i + 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i - 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(m_dst.m_i, m_dst.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            (m_maze_squeres.find(coordinate(curr.m_i - 1, curr.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                            (m_maze_squeres.find(coordinate(curr.m_i + 2, curr.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            current_pos.m_mid = curr;
                            current_pos.m_head = coordinate(curr.m_i - 1, curr.m_j);
                        }
                    }
                    else
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        if (current_pos.m_head.m_j > current_pos.m_mid.m_j)
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j - 2))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j + 2))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i + 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i - 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");

                        if (curr == m_dst)
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            path_cpy = path;
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i + 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i - 1, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                            (m_maze_squeres.find(coordinate(m_dst.m_i, m_dst.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            (m_maze_squeres.find(coordinate(curr.m_i + 1, curr.m_j))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                            (m_maze_squeres.find(coordinate(curr.m_i - 2, curr.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            current_pos.m_mid = curr;
                            current_pos.m_head = coordinate(curr.m_i + 1, curr.m_j);
                        }
                    }
                }
                else
                {
                    if (curr.m_j < current_pos.m_mid.m_j)
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        if (current_pos.m_head.m_i > current_pos.m_mid.m_i)
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i - 2, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i + 2, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");

                        if (curr == m_dst)
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            path_cpy = path;
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                            (m_maze_squeres.find(coordinate(m_dst.m_i, m_dst.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j - 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                            (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j + 2))->second)->setStyleSheet("QLabel { background-color : black;}");
                            current_pos.m_mid = curr;
                            current_pos.m_head = coordinate(curr.m_i, curr.m_j - 1);
                        }
                    }
                    else
                    {
                        QTest::qWait(150);
                        if (!m_there_is_path)
                            return;
                        if (current_pos.m_head.m_i > current_pos.m_mid.m_i)
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i - 2, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_head.m_i + 2, current_pos.m_head.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                        (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");

                        if (curr == m_dst)
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            path_cpy = path;
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j + 1))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j - 1))->second)->setStyleSheet("QLabel { background-color : black;}");
                            (m_maze_squeres.find(coordinate(current_pos.m_mid.m_i, current_pos.m_mid.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                            (m_maze_squeres.find(coordinate(m_dst.m_i, m_dst.m_j))->second)->setStyleSheet("QLabel { background-color : green;}");
                        }
                        else
                        {
                            QTest::qWait(150);
                            if (!m_there_is_path)
                                return;
                            (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j + 1))->second)->setStyleSheet("QLabel { background-color : yellow;}");
                            (m_maze_squeres.find(coordinate(curr.m_i, curr.m_j - 2))->second)->setStyleSheet("QLabel { background-color : black;}");
                            current_pos.m_mid = curr;
                            current_pos.m_head = coordinate(curr.m_i, curr.m_j + 1);
                        }
                    }
                }
            }
        }
        if (path_cpy.empty())
            path_cpy = path;
    }
}

maze::maze(int width, int height, const std::vector<coordinate> obstacles, QWidget *parent)
    : QWidget(parent), m_maze_width(width), m_maze_height(height), m_obstacles(obstacles), m_src(coordinate(-1, -1)), m_dst(coordinate(-1, -1)), m_there_is_path(0)
{
    QHBoxLayout** horizonatals;
    horizonatals = new QHBoxLayout*[m_maze_height];
    QVBoxLayout* verticals = new QVBoxLayout(this);
    verticals->setSpacing(1);

    m_maze = new int*[m_maze_height];
    for (int i = 0; i < m_maze_height; ++i)
    {
        horizonatals[i] = new QHBoxLayout;
        horizonatals[i]->setSpacing(1);
        m_maze[i] = new int[m_maze_width];

        for(int j = 0; j < m_maze_width; ++j)
        {
            m_maze[i][j] = 0;
            squere* curr = new squere(coordinate(i,j));
            connect(curr, SIGNAL(source_changed(coordinate)), this, SLOT(on_source_changed(coordinate)));
            connect(curr, SIGNAL(destination_changed(coordinate)), this, SLOT(on_destination_changed(coordinate)));
            horizonatals[i]->addWidget(curr);
            m_maze_squeres.insert(std::make_pair(coordinate(i, j), curr));
        }
        verticals->addLayout(horizonatals[i]);
    }

    std::vector<coordinate>::iterator it = m_obstacles.begin();
    for (it; it != m_obstacles.end(); ++it)
    {
        m_maze[it->m_i][it->m_j] = -1;
        (m_maze_squeres.find(*it)->second)->setStyleSheet("QLabel { background-color : red;}");
    }
    setLayout(verticals);
    resize(500,500);
}

maze::~maze()
{
    for (int i = 0; i < m_maze_height; ++i)
    {
        delete m_maze[i];
    }
    delete[] m_maze;
}

void maze::set_width(const int & width)
{
    m_maze_width = width;
}

void maze::set_height(const int & height)
{
    m_maze_height = height;
}

void maze::set_src(const coordinate & src)
{
    m_src = src;
}

void maze::set_dst(const coordinate & dst)
{
    m_dst = dst;
}

void maze::set_obstacles(const std::vector<coordinate>& obst)
{
    m_obstacles = obst;
    std::vector<coordinate>::iterator it = m_obstacles.begin();
    for (it; it != m_obstacles.end(); ++it)
    {
        m_maze[it->m_i][it->m_j] = -1;
    }
}

void maze::set_there_is_path(bool there_is_path)
{
    m_there_is_path = there_is_path;
}

int maze::get_width()
{
    return m_maze_width;
}

int maze::get_height()
{
    return m_maze_height;
}

bool maze::get_there_is_path()
{
    return m_there_is_path;
}

coordinate maze::get_src()
{
    return m_src;
}

coordinate maze::get_dst()
{
    return m_dst;
}

std::vector<coordinate> maze::get_obstacles()
{
    return m_obstacles;
}

void maze::on_source_changed(coordinate src)
{
    m_ship_1.m_head = coordinate(-1, -1);
    m_ship_1.m_mid = coordinate(-1, -1);

    m_ship_2.m_head = coordinate(-1, -1);
    m_ship_2.m_mid = coordinate(-1, -1);

    if(!is_available(src))
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Place is not availabe !");
        messageBox.setFixedSize(500,200);
        while(!m_path.empty())
        {
            (m_maze_squeres.find(m_path.top())->second)->setStyleSheet("QLabel { background-color : green;}");
            m_path.pop();
        }
    }
    else
    {
        if (is_available(coordinate(src.m_i - 1, src.m_j)) && is_available(coordinate(src.m_i + 1, src.m_j)))
        {
            m_ship_1.m_head = coordinate(src.m_i - 1, src.m_j);
            m_ship_1.m_mid = src;
            m_src = src;
            m_ship_2 = m_ship_1;
        }

        if(m_ship_1.m_head.m_i != -1)
        {
            if(!is_rotateable(m_ship_1))
            {
                if (is_available(coordinate(src.m_i, src.m_j - 1)) && is_available(coordinate(src.m_i, src.m_j + 1)))
                {
                    m_ship_2.m_head = coordinate(src.m_i, src.m_j - 1);
                    m_ship_2.m_mid = src;
                    m_src = src;
                }
            }
            else
            {
                m_ship_2 = m_ship_1;
                m_src = src;
            }
        }
        else
        {
            if (is_available(coordinate(src.m_i, src.m_j - 1)) && is_available(coordinate(src.m_i, src.m_j + 1)))
            {
                m_ship_2.m_head = coordinate(src.m_i, src.m_j - 1);
                m_ship_2.m_mid = src;
                m_ship_1 = m_ship_2;
                m_src = src;
            }
        }
        if(m_ship_1.m_head.m_i == -1 && m_ship_2.m_head.m_i == -1)
        {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "There is no place for ship!");
            messageBox.setFixedSize(500,200);
            while(!m_path.empty())
            {
                (m_maze_squeres.find(m_path.top())->second)->setStyleSheet("QLabel { background-color : green;}");
                m_path.pop();
            }
        }
        else
        {
            if (m_dst.m_i != -1)
            {
                for (int i = 0; i < m_maze_height; ++i)
                {
                    for(int j = 0; j < m_maze_width; ++j)
                    {
                        m_maze[i][j] = 0;
                        (m_maze_squeres.find(coordinate(i,j))->second)->setStyleSheet("QLabel { background-color : black;}");
                    }
                }

                std::vector<coordinate>::iterator it = m_obstacles.begin();
                for (it; it != m_obstacles.end(); ++it)
                {
                    m_maze[it->m_i][it->m_j] = -1;
                    (m_maze_squeres.find(*it)->second)->setStyleSheet("QLabel { background-color : red;}");
                }
                find_path();
                if (m_path.empty())
                {
                    QMessageBox messageBox;
                    messageBox.critical(0, "Error", "There is no path!");
                    messageBox.setFixedSize(500,200);
                    m_there_is_path = 0;
                    return;
                }
                m_there_is_path = 1;
                std::stack<coordinate> path = m_path;
                while(!m_path.empty())
                {
                    (m_maze_squeres.find(m_path.top())->second)->setStyleSheet("QLabel { background-color : green;}");
                    m_path.pop();
                }
                start_ship_movement(path);
                return;
            }
        }
    }
}

void maze::on_destination_changed(coordinate dst)
{
    if(!is_available(dst))
    {
        QMessageBox messageBox;
        messageBox.critical(0, "Error", "Place is not availabe !");
        messageBox.setFixedSize(500,200);
        while(!m_path.empty())
        {
            (m_maze_squeres.find(m_path.top())->second)->setStyleSheet("QLabel { background-color : green;}");
            m_path.pop();
        }
    }
    else
    {
        m_dst = dst;
        if (m_src.m_i != -1)
        {
            for (int i = 0; i < m_maze_height; ++i)
            {
                for(int j = 0; j < m_maze_width; ++j)
                {
                    m_maze[i][j] = 0;
                    (m_maze_squeres.find(coordinate(i,j))->second)->setStyleSheet("QLabel { background-color : black;}");
                }
            }

            std::vector<coordinate>::iterator it = m_obstacles.begin();
            for (it; it != m_obstacles.end(); ++it)
            {
                m_maze[it->m_i][it->m_j] = -1;
                (m_maze_squeres.find(*it)->second)->setStyleSheet("QLabel { background-color : red;}");
            }
            find_path();
            if (m_path.empty())
            {
                QMessageBox messageBox;
                messageBox.critical(0, "Error", "There is no path!");
                messageBox.setFixedSize(500,200);
                m_there_is_path = 0;
                return;
            }
            m_there_is_path = 1;
            std::stack<coordinate> path = m_path;
            while(!m_path.empty())
            {
                (m_maze_squeres.find(m_path.top())->second)->setStyleSheet("QLabel { background-color : green;}");
                m_path.pop();
            }
            start_ship_movement(path);
            return;
        }
    }
}

