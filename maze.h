#ifndef MAZE_H
#define MAZE_H

#include <QWidget>
#include <QFuture>
#include <stack>

#include "ship.h"
#include "coordinate.h"
#include "squere.h"

class maze : public QWidget
{
    Q_OBJECT

private:
    int** m_maze;
    int m_maze_width;
    int m_maze_height;


    std::map<coordinate, squere*> m_maze_squeres;
    std::vector<coordinate> m_obstacles;

    ship m_ship_1;
    ship m_ship_2;

    coordinate m_src;
    coordinate m_dst;

    std::stack<coordinate> m_path;

    QFuture<void> m_thread_for_movement;

    bool m_there_is_path;
    bool is_available(const coordinate&);
    bool is_rotateable(const ship&);
    bool find_path();
    bool path_finding_core(const ship&);
    std::vector<coordinate> get_dst_neighbours();
    bool check_neighbour(const coordinate&, const coordinate&);
    void start_ship_movement(const std::stack<coordinate>&);

public:
    maze(int width, int height, const std::vector<coordinate> obstacles, QWidget *parent = 0);
    ~maze();
    void set_width(const int&);
    void set_height(const int&);
    void set_src(const coordinate&);
    void set_dst(const coordinate&);
    void set_obstacles(const std::vector<coordinate> &);
    void set_there_is_path(bool);

    int get_width();
    int get_height();
    bool get_there_is_path();
    coordinate get_src();
    coordinate get_dst();
    std::vector<coordinate> get_obstacles();

public slots:
    void on_source_changed(coordinate);
    void on_destination_changed(coordinate);
};

#endif // MAZE_H
